package uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents;


import java.io.File;

import cn.Ragnarok.BitmapFilter;
import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.ImageFilterHandler;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilterComponents.AsyncBmpFilter;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilterComponents.AsyncImageSaveFilter;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilterComponents.AsyncThumbnailFilter;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilterComponents.DataToFilter;

public class ImageFilter {
    public static final int RAW = 9999;

    public static final Model filters[] = {
            new Model(RAW, "Raw Style"),
            new Model(BitmapFilter.OIL_STYLE, "Oil Style"),
            new Model(BitmapFilter.GRAY_STYLE, "Gray Style"),
            new Model(BitmapFilter.INVERT_STYLE, "Invert Style"),
            new Model(BitmapFilter.OLD_STYLE, "Old Style"),
            new Model(BitmapFilter.NEON_STYLE, "Neon Style"),
            new Model(BitmapFilter.TV_STYLE, "TV Style"),
            new Model(BitmapFilter.HDR_STYLE, "HDR Style")
    };

    public static String getEffectName(int i) {
        return filters[i].name;
    }

    public static int getAmountOfFilters() {
        return filters.length;
    }

    public void getFilteredThumbnail(File photo, ImageFilterHandler handler, int position) {
        new AsyncThumbnailFilter(handler).execute(new DataToFilter(photo, filters[position].filter));
    }

    public void getFilteredImage(File file, ImageFilterHandler handler, int position,
                                 int width, int height) {
        new AsyncBmpFilter(handler).execute(
                new DataToFilter(file,
                        filters[position].filter,
                        width,
                        height));
    }

    public void getFilteredImageToSave(File file, ImageFilterHandler handler, int filterType) {
        new AsyncImageSaveFilter(handler).execute(new DataToFilter(file, filters[filterType].filter));
    }

    public static class Model {
        public Model(int filter, String name) {
            this.filter = filter;
            this.name = name;
        }

        public int filter;
        public String name;
    }
}
