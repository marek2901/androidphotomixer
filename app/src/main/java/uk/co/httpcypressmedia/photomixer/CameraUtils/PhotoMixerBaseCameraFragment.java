package uk.co.httpcypressmedia.photomixer.CameraUtils;

import android.util.Log;

import com.commonsware.cwac.camera.CameraFragment;
import com.commonsware.cwac.camera.SimpleCameraHost;

public class PhotoMixerBaseCameraFragment extends CameraFragment {

    public static final String TAG = PhotoMixerBaseCameraFragment.class.getSimpleName();

    public void safeAutoFocus() {
        try {
            if (isAutoFocusAvailable())
                autoFocus();
        }catch (RuntimeException e){
//            auto focus failed to nothing
            Log.d(TAG, e.getMessage());
        }
    }

    public void safeTakeAPicture(){
        try {
            takePicture();
        }catch (IllegalStateException e){
            Log.e(TAG,e.getMessage());
            // cant do picture while autofocus
        }

    }
}
