package uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.ImageFilterHandler;
import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.PreviewsClickListener;
import uk.co.httpcypressmedia.photomixer.R;

public class PreviewsAdapter extends BaseAdapter {
    public static final String TAG = PreviewsAdapter.class.getSimpleName();
    Context context;
    File photo;

    public PreviewsAdapter(Context context, File photo) {
        this.context = context;
        this.photo = photo;
    }

    @Override
    public int getCount() {
        return ImageFilter.getAmountOfFilters();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View returnView = view;
        ViewHolder holder;
        if (view == null) {
            returnView = LayoutInflater.from(context).inflate(R.layout.editor_preview_item, null);
            holder = new ViewHolder(returnView);
            returnView.setTag(holder);
        } else {
            holder = (ViewHolder) returnView.getTag();
        }

        holder.fillTitle(ImageFilter.getEffectName(i));
        holder.loadImage(i);

        return returnView;
    }

    class ViewHolder implements ImageFilterHandler {

        @Bind(R.id.previewImageView)
        ImageView imageViewPreview;
        @Bind(R.id.previewTextView)
        TextView title;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        public void fillTitle(String title) {
            this.title.setText(title);
        }

        public void loadImage(int position) {
            new ImageFilter().getFilteredThumbnail(photo, this, position);
        }

        @Override
        public void imageFiltered(Bitmap image) {
            imageViewPreview.setImageBitmap(image);
        }

    }
}
