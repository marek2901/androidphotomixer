package uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilterComponents;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import java.io.File;

import cn.Ragnarok.BitmapFilter;
import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.ImageFilterHandler;
import uk.co.httpcypressmedia.photomixer.Utils.BitmapUtils;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilter;

public class AsyncThumbnailFilter extends AsyncTask<DataToFilter, Void, Bitmap> {
    ImageFilterHandler handler;

    private Bitmap getBitMapFromFile(File file) {

        return BitmapUtils.loadEfficientImageFromFile(file, 70, 70);
    }

    public AsyncThumbnailFilter(ImageFilterHandler handler) {
        this.handler = handler;
    }

    @Override
    protected Bitmap doInBackground(DataToFilter... dataToFilters) {
        DataToFilter dataToFilter = dataToFilters[0];

        if (dataToFilter.BitmapFilter != ImageFilter.RAW)
            return BitmapFilter.changeStyle(getBitMapFromFile(
                            dataToFilter.datafile),
                    dataToFilter.BitmapFilter);

        else return getBitMapFromFile(
                dataToFilter.datafile);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        handler.imageFiltered(bitmap);
    }
}