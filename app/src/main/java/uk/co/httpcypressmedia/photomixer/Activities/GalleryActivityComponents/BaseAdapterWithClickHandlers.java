package uk.co.httpcypressmedia.photomixer.Activities.GalleryActivityComponents;

import android.util.Log;
import android.view.View;

import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.GalleryClickHandler;

public abstract class BaseAdapterWithClickHandlers extends AdapterWithSelectionManager
        implements View.OnClickListener {

    GalleryClickHandler galleryClickHandler;

    private boolean selectionMode = false;

    public BaseAdapterWithClickHandlers(GalleryClickHandler galleryClickHandler) {
        this.galleryClickHandler = galleryClickHandler;
    }

    public void setSelectionMode(boolean selectionMode) {
        this.selectionMode = selectionMode;
        clearSelection();
    }

    @Override
    public void onClick(View view) {
        if (selectionMode) {
            selectionClick(view);
        } else {
            normalClick(view);
        }
    }

    public void normalClick(View view) {
        try {
            GalleryViewHolder holder = (GalleryViewHolder) view.getTag();
            galleryClickHandler.onClickedItem(holder.getPhotoFile());
        } catch (NullPointerException e) {
            Log.e(TAG, "File not exist: " + e.getMessage());
        }
    }

    public void selectionClick(View view) {
        try {
            GalleryViewHolder holder = (GalleryViewHolder) view.getTag();
            selectItem(holder);
        } catch (NullPointerException e) {
            Log.e(TAG, "File not exist: " + e.getMessage());
        }
    }
}
