package uk.co.httpcypressmedia.photomixer.Activities.GalleryActivityComponents;

import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import uk.co.httpcypressmedia.photomixer.R;

public abstract class GalleryActionMode implements ActionMode.Callback {
    Toolbar toolbar;

    public GalleryActionMode(Toolbar toolbar) {
        this.toolbar = toolbar;
        toolbar.startActionMode(this);
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        actionMode.getMenuInflater().inflate(R.menu.gallery_contextual, menu);
        actionModeStarted();
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.action_delete) {
            deleteClicked();
            return true;
        } else {
            onDestroyActionMode(actionMode);
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        toolbar.collapseActionView();
        actionModeEnded();
    }

    public abstract void deleteClicked();
    public abstract void actionModeStarted();
    public abstract void actionModeEnded();
}
