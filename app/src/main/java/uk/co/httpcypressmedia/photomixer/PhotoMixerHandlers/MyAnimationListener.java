package uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers;

import android.animation.Animator;

/**
 * Created by marek on 26.07.15.
 * Dead Simple animation Listener
 */
public abstract class MyAnimationListener implements Animator.AnimatorListener{
    @Override
    public void onAnimationStart(Animator animator) {

    }

    @Override
    public void onAnimationEnd(Animator animator) {
        onAnimationEnded(animator);
    }

    @Override
    public void onAnimationCancel(Animator animator) {

    }

    @Override
    public void onAnimationRepeat(Animator animator) {

    }

    public abstract void onAnimationEnded(Animator animator);
}
