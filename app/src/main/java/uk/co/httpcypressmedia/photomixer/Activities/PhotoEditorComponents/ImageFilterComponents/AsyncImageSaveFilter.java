package uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilterComponents;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.File;

import cn.Ragnarok.BitmapFilter;
import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.ImageFilterHandler;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilter;

public class AsyncImageSaveFilter extends AsyncTask<DataToFilter, Void, Bitmap> {
    ImageFilterHandler handler;

    private Bitmap getBitMapFromFile(File file) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(file.getAbsolutePath(), options);
    }

    public AsyncImageSaveFilter(ImageFilterHandler handler) {
        this.handler = handler;
    }

    @Override
    protected Bitmap doInBackground(DataToFilter... dataToFilters) {
        DataToFilter dataToFilter = dataToFilters[0];

        if (dataToFilter.BitmapFilter != ImageFilter.RAW)
            return BitmapFilter.changeStyle(getBitMapFromFile(
                            dataToFilter.datafile),
                    dataToFilter.BitmapFilter);

        else return getBitMapFromFile(
                dataToFilter.datafile);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        handler.imageFiltered(bitmap);
    }
}