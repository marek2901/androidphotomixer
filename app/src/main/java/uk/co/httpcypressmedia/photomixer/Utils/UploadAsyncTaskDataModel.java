package uk.co.httpcypressmedia.photomixer.Utils;

import android.graphics.Bitmap;

import java.io.File;

public class UploadAsyncTaskDataModel {
    public UploadAsyncTaskDataModel(File file, Bitmap bitmap) {
        this.file = file;
        this.bitmap = bitmap;
    }

    public File file;
    public Bitmap bitmap;
}
