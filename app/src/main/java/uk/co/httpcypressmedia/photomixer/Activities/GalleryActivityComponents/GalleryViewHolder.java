package uk.co.httpcypressmedia.photomixer.Activities.GalleryActivityComponents;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.httpcypressmedia.photomixer.R;

public class GalleryViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.gallery_item_image_view)
    ImageView galleryImageView;

    @Bind(R.id.gallery_item_text_view)
    TextView galleryTextView;

    @Bind(R.id.item_back_ground)
    RelativeLayout backGround;

    File photoFile;

    public GalleryViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setBackGroundClickListeners(View.OnClickListener listener){
        backGround.setOnClickListener(listener);
        backGround.setTag(this);
    }

    public void fillFields(File file, Context context) {
        this.photoFile = file;
        galleryTextView.setText(file.getName());
        Picasso.with(context).load(file).memoryPolicy(MemoryPolicy.NO_STORE).into(galleryImageView);
    }

    public RelativeLayout getBackGround() {
        return backGround;
    }


    public File getPhotoFile() {
        return photoFile;
    }
}
