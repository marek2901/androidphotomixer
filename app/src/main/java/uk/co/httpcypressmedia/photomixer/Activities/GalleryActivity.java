package uk.co.httpcypressmedia.photomixer.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.httpcypressmedia.photomixer.PhotoMixerEventBus.BusProvider;
import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.GalleryClickHandler;
import uk.co.httpcypressmedia.photomixer.R;
import uk.co.httpcypressmedia.photomixer.Utils.AdMobUtils;
import uk.co.httpcypressmedia.photomixer.Activities.GalleryActivityComponents.GalleryActionMode;
import uk.co.httpcypressmedia.photomixer.Activities.GalleryActivityComponents.GalleryAdapter;

public class GalleryActivity extends PhotoMixerMaterialToolbarActivity
        implements GalleryClickHandler {

    @Bind(R.id.tool_bar)
    Toolbar toolbar;

    @Bind(R.id.galleryView)
    RecyclerView recyclerView;

    private GalleryAdapter galleryAdapter;

    @Bind(R.id.adView)
    AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);
        prepareToolbar(toolbar);
        prepareRecyclerView();
        new AdMobUtils().showAdsBanner(adView);
    }

    private void prepareRecyclerView() {
        recyclerView.setLayoutManager(new GridLayoutManager(GalleryActivity.this, 2));
        recyclerView.setHasFixedSize(true);
        galleryAdapter = new GalleryAdapter(GalleryActivity.this, this);
        recyclerView.setAdapter(galleryAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_settings:
                new GalleryActionMode(toolbar) {
                    @Override
                    public void deleteClicked() {
                        galleryAdapter.deleteSelectedItems();
                    }

                    @Override
                    public void actionModeStarted() {
                        galleryAdapter.setSelectionMode(true);
                    }

                    @Override
                    public void actionModeEnded() {
                        galleryAdapter.setSelectionMode(false);
                    }
                };
            default:
                return true;
        }
    }

    @Override
    public void onClickedItem(File clickedPhotoFile) {
        Intent intent = new Intent(GalleryActivity.this, PhotoEditor.class);
        intent.putExtra(File.class.getSimpleName(), new Gson().toJson(clickedPhotoFile));
        startActivity(intent);
    }

    @Subscribe
    public void newPhoto(String photoTaken) {
        galleryAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        galleryAdapter.dataChanged();
        BusProvider.getInstance().register(this);
    }
}
