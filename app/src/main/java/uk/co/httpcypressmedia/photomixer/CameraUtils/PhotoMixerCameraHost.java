package uk.co.httpcypressmedia.photomixer.CameraUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;

import com.commonsware.cwac.camera.CameraUtils;
import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;

import java.io.File;
import java.util.List;

import uk.co.httpcypressmedia.photomixer.PhotoMixerEventBus.BusProvider;

public class PhotoMixerCameraHost extends SimpleCameraHost {

    public static final String PHOTO_TAKEN = "photoTaken";

    boolean useFFC;

    public PhotoMixerCameraHost(Context _ctxt, boolean useFFc) {
        super(_ctxt);
        this.useFFC = useFFc;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    protected File getPhotoDirectory() {
        File path = PhotoDirUtils.getPhotoDir();
        path.mkdir();
        return path;
    }

    @Override
    protected boolean useFrontFacingCamera() {
        return useFFC;
    }

    @Override
    public void saveImage(PictureTransaction xact, byte[] image) {
        super.saveImage(xact, image);
        photoHasBeenTaken();

    }

    void photoHasBeenTaken() {
        BusProvider.getInstance().post(PHOTO_TAKEN);
    }

    @Override
    public Camera.Size getPictureSize(PictureTransaction xact, Camera.Parameters parameters) {
        Camera.Size bestSize = null;
        List<Camera.Size> sizeList = parameters.getSupportedPreviewSizes();

        bestSize = sizeList.get(0);

        for(int i = 1; i < sizeList.size(); i++){
            if((sizeList.get(i).width * sizeList.get(i).height) >
                    (bestSize.width * bestSize.height)){
                bestSize = sizeList.get(i);
            }
        }
        return bestSize;
    }
}
