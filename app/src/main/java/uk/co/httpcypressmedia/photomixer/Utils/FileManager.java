package uk.co.httpcypressmedia.photomixer.Utils;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.FileOperationHandler;
import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.ImageFilterHandler;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilter;

@SuppressWarnings("unchecked")
public class FileManager {

    public static final String TAG = FileManager.class.getSimpleName();


    public void deleteFile(File file, FileOperationHandler handler) {
        ArrayList<File> files = new ArrayList<>();
        files.add(file);
        try {
            if (files.size() == 1)
                new DeleteFilesAsync(handler).execute(files);
        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void uploadFile(final File file, int filterType, final FileOperationHandler handler) {
        try {
            new ImageFilter().getFilteredImageToSave(file, new ImageFilterHandler() {
                @Override
                public void imageFiltered(Bitmap image) {
                    new UploadFilesAsync(handler).execute(new UploadAsyncTaskDataModel(
                            file, image
                    ));
                }
            }, filterType);


        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
        }
    }


    public void deleteFiles(ArrayList<File> files, FileOperationHandler handler) {
        try {
            if (files.size() > 0)
                new DeleteFilesAsync(handler).execute(files);
        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    class DeleteFilesAsync extends AsyncTask<ArrayList<File>, Void, Boolean> {
        FileOperationHandler handler;

        public DeleteFilesAsync(FileOperationHandler handler) {
            this.handler = handler;
        }

        @SafeVarargs
        @Override
        protected final Boolean doInBackground(ArrayList<File>... arrayLists) {
            try {
                ArrayList<File> files = arrayLists[0];
                for (File file : files) {
                    file.delete();
                }
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            handler.operationCompleted(aBoolean);
        }
    }


    @SuppressWarnings("ResultOfMethodCallIgnored")
    class UploadFilesAsync extends AsyncTask<UploadAsyncTaskDataModel, Void, Boolean> {
        FileOperationHandler handler;

        public UploadFilesAsync(FileOperationHandler handler) {
            this.handler = handler;
        }

        @Override
        protected final Boolean doInBackground(UploadAsyncTaskDataModel... files) {
            try {
                File file = files[0].file;

                Bitmap bitmap = files[0].bitmap;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();

                FileOutputStream fos = new FileOutputStream(file);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();

                return true;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            handler.operationCompleted(aBoolean);
        }
    }


}
