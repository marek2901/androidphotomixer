package uk.co.httpcypressmedia.photomixer.Activities.GalleryActivityComponents;

import android.util.SparseBooleanArray;

public abstract class AdapterWithSelectionManager extends BaseAdapterWithFileManagement {
    public static final String TAG = AdapterWithSelectionManager.class.getSimpleName();
    SparseBooleanArray selectedItems = new SparseBooleanArray();

    private void synchronizeViewSelection(GalleryViewHolder galleryViewHolder, boolean selected) {
        galleryViewHolder.getBackGround().setSelected(selected);
    }

    public void synchronizeViewSelection(GalleryViewHolder galleryViewHolder, int position) {
        boolean selected = selectedItems.get(position, false);
        galleryViewHolder.getBackGround().setSelected(selected);
    }

    public void selectItem(GalleryViewHolder holder) {
        if (selectedItems.get(holder.getAdapterPosition(), false)) {
            selectedItems.delete(holder.getAdapterPosition());
            synchronizeViewSelection(holder, false);
        } else {
            selectedItems.put(holder.getAdapterPosition(), true);
            synchronizeViewSelection(holder, true);
        }
    }

    public void deleteSelectedItems() {
        super.deleteSelectedItems(selectedItems);
        selectedItems.clear();
    }

    public void clearSelection(){
        selectedItems.clear();
        notifyDataSetChanged();
    }
}
