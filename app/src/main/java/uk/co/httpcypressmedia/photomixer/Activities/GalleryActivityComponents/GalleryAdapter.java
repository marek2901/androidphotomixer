package uk.co.httpcypressmedia.photomixer.Activities.GalleryActivityComponents;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.GalleryClickHandler;
import uk.co.httpcypressmedia.photomixer.R;


public class GalleryAdapter extends BaseAdapterWithClickHandlers {
    Context context;

    public GalleryAdapter(Context context, GalleryClickHandler galleryClickHandler) {
        super(galleryClickHandler);
        this.context = context;
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.
                from(viewGroup.getContext()).inflate(R.layout.gallery_item, viewGroup, false);
        GalleryViewHolder galleryViewHolder = new GalleryViewHolder(view);
        galleryViewHolder.setBackGroundClickListeners(this);
        return galleryViewHolder;
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder galleryViewHolder, int i) {
        galleryViewHolder.fillFields(files.get(i), context);
        synchronizeViewSelection(galleryViewHolder, i);
    }

    @Override
    public int getItemCount() {
        try {
            return files.size();
        } catch (NullPointerException e) {
            return 0;
        }
    }
}
