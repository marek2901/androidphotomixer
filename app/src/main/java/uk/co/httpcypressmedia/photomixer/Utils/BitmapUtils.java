package uk.co.httpcypressmedia.photomixer.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;

public class BitmapUtils {

    public static Bitmap loadEfficientImageFromFile(File fileObject, int requiredWidth, int requiredHeight) {

        Bitmap imageBitmap = null;
        FileInputStream fileInputStream = null;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
/*
* Setting 'inJustDecodeBounds' as true, return no bitmap on decode, but
* allow to query the bitmap detail without allocating the memory.
*/
        bitmapOptions.inJustDecodeBounds = true;

        try {

            fileInputStream = new FileInputStream(fileObject.getAbsolutePath());
            BitmapFactory.decodeStream(fileInputStream, null, bitmapOptions);
            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

// Retrieve Bitmap Original Width and Height
        int bitmapOriginalWidth = bitmapOptions.outWidth;
        int bitmapOriginalHeight = bitmapOptions.outHeight;

        int scaleFactor = 1;

// Find the scaling factor of the image
        while (bitmapOriginalWidth * 0.9  > requiredWidth
                && bitmapOriginalHeight * 0.9 > requiredHeight) {

            bitmapOriginalWidth /= 2;
            bitmapOriginalHeight /= 2;

            scaleFactor *= 2;
        }

        BitmapFactory.Options finalBitmapOptions = new BitmapFactory.Options();
/*
* Set scaleFactoer to 'inSampleSize' will requests the decoder to
* subsample the original image, returning a smaller image to save
* memory.
*/
        finalBitmapOptions.inSampleSize = scaleFactor;

        try {

            fileInputStream = new FileInputStream(fileObject.getAbsolutePath());
            imageBitmap = BitmapFactory.decodeStream(fileInputStream, null,
                    finalBitmapOptions);
            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return imageBitmap;
    }
}
