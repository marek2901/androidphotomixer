package uk.co.httpcypressmedia.photomixer.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.commonsware.cwac.camera.CameraView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.httpcypressmedia.photomixer.CameraUtils.PhotoMixerBaseCameraFragment;
import uk.co.httpcypressmedia.photomixer.R;
import uk.co.httpcypressmedia.photomixer.Activities.GalleryActivity;


@SuppressWarnings("Convert2Lambda")
public class PhotoMixerCameraFragment extends PhotoMixerBaseCameraFragment {

    @Bind(R.id.camera)
    CameraView cameraView;

    View.OnClickListener switchOnClickListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        switchOnClickListener = (View.OnClickListener) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_mixer_camera, container, false);
        ButterKnife.bind(this, view);
        setCameraView(cameraView);
        cameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                safeAutoFocus();
            }
        });
        return view;
    }

    @Bind(R.id.switch_camera_button)
    Button buttonSwitch;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buttonSwitch.setOnClickListener(switchOnClickListener);
    }

    @OnClick(R.id.take_picture_button)
    void takePictureButtonClicked() {
        safeTakeAPicture();
    }


    @OnClick(R.id.gallery_button)
    void galleryButtonClicked() {
        startActivity(new Intent(getActivity(), GalleryActivity.class));
    }


}
