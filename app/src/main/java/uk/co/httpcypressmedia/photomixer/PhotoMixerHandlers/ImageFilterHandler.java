package uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers;

import android.graphics.Bitmap;

public interface ImageFilterHandler  {
    void imageFiltered(Bitmap image);
}
