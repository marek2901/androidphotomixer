package uk.co.httpcypressmedia.photomixer.Activities.GalleryActivityComponents;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;

import java.io.File;
import java.util.ArrayList;

import uk.co.httpcypressmedia.photomixer.CameraUtils.PhotoDirUtils;
import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.FileOperationHandler;
import uk.co.httpcypressmedia.photomixer.Utils.FileManager;

public abstract class BaseAdapterWithFileManagement extends RecyclerView.Adapter<GalleryViewHolder>
        implements FileOperationHandler {
    public static final String TAG = BaseAdapterWithFileManagement.class.getSimpleName();
    ArrayList<File> files = PhotoDirUtils.getListOfPhotos();

    public void deleteSelectedItems(SparseBooleanArray selectedItems) {
        ArrayList<File> filesToDelete = new ArrayList<>();
        for (int i = 0; i < selectedItems.size(); i++) {
            int selectedItemIndex = selectedItems.keyAt(i);
            Log.e(TAG, String.valueOf(selectedItemIndex));
            filesToDelete.add(files.get(selectedItemIndex));
        }
        new FileManager().deleteFiles(filesToDelete,this);
    }

    @Override
    public void operationCompleted(boolean success) {
        dataChanged();
    }

    public void dataChanged(){
        files = PhotoDirUtils.getListOfPhotos();
        notifyDataSetChanged();
    }

}
