package uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.ImageFilterComponents;

import java.io.File;

public class DataToFilter {

    public DataToFilter(File datafile, int bitmapFilter) {
        this.datafile = datafile;
        this.BitmapFilter = bitmapFilter;
    }

    public DataToFilter(File datafile, int bitmapFilter, int width, int height) {
        this.datafile = datafile;
        this.BitmapFilter = bitmapFilter;
        this.width = width;
        this.height = height;
    }
    public File datafile;
    public int width;
    public int height;
    public int BitmapFilter;
}
