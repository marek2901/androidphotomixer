package uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;

import java.io.File;

import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.FileOperationHandler;
import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.PreviewsClickListener;
import uk.co.httpcypressmedia.photomixer.R;
import uk.co.httpcypressmedia.photomixer.Utils.FileManager;

public abstract class PhotoEditorActionMode implements ActionMode.Callback,
        AdapterView.OnItemClickListener {

    private Toolbar toolbar;

    Context context;
    File photo;

    int current;

    public void setCurrent(int current) {
        this.current = current;
    }

    ActionMode actionMode;

    public PhotoEditorActionMode(Toolbar toolbar, Context context, File photo) {
        this.context = context;
        this.photo = photo;
        this.toolbar = toolbar;
        current = 0;
    }

    public void startActionMode() {
        try {
            actionMode = toolbar.startActionMode(this);
        } catch (NullPointerException e) {
            Log.e(PhotoEditorActionMode.class.getSimpleName(), "start action mode failed toolbar was null");
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        actionMode.getMenuInflater().inflate(R.menu.editor_contextual, menu);
        actionModeStarted();
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_delete:
                delete();
                return true;
            case android.R.id.home:
                endActionMode();
                return true;
            case R.id.action_save:
                savePhoto();
            default:
                return false;
        }
    }

    public void delete() {
        new FileManager().deleteFile(photo, new FileOperationHandler() {
            @Override
            public void operationCompleted(boolean success) {
                endActionMode();
            }
        });
    }

    private void savePhoto() {
        new FileManager().uploadFile(photo, current,
                new FileOperationHandler() {
                    @Override
                    public void operationCompleted(boolean success) {
                        endActionMode();
                    }
                }
        );
    }

    public void endActionMode() {
        actionMode.finish();
    }

    public abstract void actionModeStarted();

    public abstract void actionModeEnded();


    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        actionModeEnded();
    }
}
