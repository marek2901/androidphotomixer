package uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers;

public interface FileOperationHandler {
    void operationCompleted(boolean success);
}
