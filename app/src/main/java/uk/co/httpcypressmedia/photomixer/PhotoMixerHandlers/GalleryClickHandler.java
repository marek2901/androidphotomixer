package uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers;

import java.io.File;

public interface GalleryClickHandler {
    void onClickedItem(File clickedPhotoFile);
}
