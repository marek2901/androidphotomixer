package uk.co.httpcypressmedia.photomixer.Activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import org.lucasr.twowayview.TwoWayView;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.httpcypressmedia.photomixer.R;
import uk.co.httpcypressmedia.photomixer.Utils.ShareUtils;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents.EditorActionModeBaseActivity;

@SuppressWarnings("ConstantConditions")
public class PhotoEditor extends EditorActionModeBaseActivity {

    public static final String TAG = PhotoEditor.class.getSimpleName();

    @Bind(R.id.tool_bar)
    Toolbar toolbar;

    @Bind(R.id.editor_image_view)
    SubsamplingScaleImageView mainImageView;

    @Bind(R.id.preview_list)
    TwoWayView previews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        setPhoto(getFileFromIntent(savedInstanceState));
        ButterKnife.bind(this);
        prepareToolbar(toolbar);
        prepareActionMode(toolbar, previews);
        loadMainPhoto(0);
        startInActionModeIfRequested(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_share:
                new ShareUtils().sharePhoto(getPhoto(), PhotoEditor.this);
                return true;
            case R.id.action_settings:
                startActionMode();
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_photo_editor, menu);
        return true;
    }

    @Override
    public void onNewImage(Bitmap bitmap) {
        try {
            mainImageView.setMinimumScaleType(SubsamplingScaleImageView.SCALE_TYPE_CENTER_INSIDE);
            mainImageView.setImage(ImageSource.bitmap(bitmap));
        } catch (NullPointerException e) {
            // Bitmap Is null
            onBackPressed();
        }
    }
}
