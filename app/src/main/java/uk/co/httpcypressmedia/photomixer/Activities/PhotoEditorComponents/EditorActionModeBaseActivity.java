package uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import org.lucasr.twowayview.TwoWayView;

import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.MyAnimationListener;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoEditor;

public abstract class EditorActionModeBaseActivity extends EditorBaseActivityWithPreviewManager {
    public static final String START_IN_ACTION_MODE = "Action_MODE_PhotoEditor.class";


    private static PhotoEditorActionMode actionMode;

    public void prepareActionMode(Toolbar toolbar, final TwoWayView previews) {
        actionMode = new PhotoEditorActionMode(toolbar, getApplicationContext(), getPhoto()) {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                previews.setItemChecked(i,true);
                setCurrent(i);
                loadMainPhoto(i);
            }

            @Override
            public void actionModeStarted() {
                previews.setVisibility(View.VISIBLE);
                ObjectAnimator.ofFloat(previews,"alpha",0f,1f).start();
                previews.setAdapter(new PreviewsAdapter(
                        getApplicationContext(), getPhoto()));
                previews.setOnItemClickListener(this);
                previews.setItemChecked(0,true);
            }

            @Override
            public void actionModeEnded() {
                ObjectAnimator animator = ObjectAnimator.ofFloat(previews, "alpha", 1f, 0f);
                animator.addListener(new MyAnimationListener() {
                    @Override
                    public void onAnimationEnded(Animator animator) {
                        previews.setVisibility(View.GONE);
                        loadMainPhoto(0);
                    }
                });
                animator.start();
            }


        };
    }

    public void startInActionModeIfRequested(Bundle savedInstanceState) {
        try {
            Boolean start = null;
            if (savedInstanceState == null) {
                Bundle extras = getIntent().getExtras();
                if (extras != null) {
                    start = extras.getBoolean(PhotoEditor.START_IN_ACTION_MODE);
                }
            } else {
                start = (boolean) savedInstanceState.getSerializable(PhotoEditor.START_IN_ACTION_MODE);
            }
            if (start != null && start)
                actionMode.startActionMode();
        }catch (NullPointerException e){
            Log.e(TAG,"Extras were null skipped auto action mode start");
        }
    }

    public void startActionMode() {
        actionMode.startActionMode();
    }
}
