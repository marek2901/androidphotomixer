package uk.co.httpcypressmedia.photomixer.PhotoMixerEventBus;

import com.squareup.otto.Bus;

public final class BusProvider {

    private static final PhotoMixerBus BUS = new PhotoMixerBus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {
        // No instances.
    }

}
