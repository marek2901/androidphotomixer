package uk.co.httpcypressmedia.photomixer.CameraUtils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class PhotoDirUtils {
    public static final String PHOTO_MIXER = "PhotoMixer";


    public static File getPhotoDir() {
        String dcimPath =
                Environment.
                        getExternalStoragePublicDirectory
                                (Environment.DIRECTORY_DCIM).getAbsolutePath();
        return new File(dcimPath + File.separator + PHOTO_MIXER);
    }

    public static ArrayList<File> getListOfPhotos() {
        Log.d("Files", "Path: " + getPhotoDir().getPath());
        try {
            return new ArrayList<>(Arrays.asList(getPhotoDir().listFiles()));
        } catch (NullPointerException e) {
            // When dir not exist (it's not yet created) method list files throw NullPointer
            // so method getListOfPhotos returns null
            return null;
        }
    }

    public static File getNewestPhoto() {
        File[] files = getPhotoDir().listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile;
    }
}
