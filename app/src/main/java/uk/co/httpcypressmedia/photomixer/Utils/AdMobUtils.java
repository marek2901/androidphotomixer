package uk.co.httpcypressmedia.photomixer.Utils;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class AdMobUtils {
    public void showAdsBanner(AdView adView) {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("5F176F9B9522AB04FE324882723BABF5")
                .addTestDevice("F1F0806F7450EAC2F7909F4B180BF779")
                .build();
        adView.loadAd(adRequest);
    }
}
