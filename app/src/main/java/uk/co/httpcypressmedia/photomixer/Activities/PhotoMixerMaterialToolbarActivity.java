package uk.co.httpcypressmedia.photomixer.Activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class PhotoMixerMaterialToolbarActivity extends AppCompatActivity{
    @SuppressWarnings("ConstantConditions")
    public void prepareToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
