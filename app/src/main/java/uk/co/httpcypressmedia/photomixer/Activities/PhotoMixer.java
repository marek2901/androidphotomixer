package uk.co.httpcypressmedia.photomixer.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.commonsware.cwac.camera.CameraHost;
import com.commonsware.cwac.camera.CameraHostProvider;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import uk.co.httpcypressmedia.photomixer.CameraUtils.PhotoDirUtils;
import uk.co.httpcypressmedia.photomixer.CameraUtils.PhotoMixerCameraHost;
import uk.co.httpcypressmedia.photomixer.PhotoMixerEventBus.BusProvider;
import uk.co.httpcypressmedia.photomixer.R;
import uk.co.httpcypressmedia.photomixer.Utils.AdMobUtils;
import uk.co.httpcypressmedia.photomixer.Fragments.PhotoMixerCameraFragment;


public class PhotoMixer extends AppCompatActivity implements
        CameraHostProvider, View.OnClickListener {

    private boolean useFFC = false;

    @Bind(R.id.adView)
    AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_mixer);
        ButterKnife.bind(this);
        commitCameraFragment();
        new AdMobUtils().showAdsBanner(adView);
    }

    private void commitCameraFragment() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new PhotoMixerCameraFragment())
                .commit();
    }

    @Override
    public CameraHost getCameraHost() {
        return new PhotoMixerCameraHost(this,useFFC);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void photoTaken(String photoTaken) {
        Intent intent = new Intent(this, PhotoEditor.class);
        intent.putExtra(File.class.getSimpleName(), new Gson().toJson(PhotoDirUtils.getNewestPhoto()));
        intent.putExtra(PhotoEditor.START_IN_ACTION_MODE,true);
        startActivity(intent);
    }

    @Bind(R.id.container)
    FrameLayout container;
    @Override
    public void onClick(View view) {
        container.removeAllViewsInLayout();
        useFFC = !useFFC;
        commitCameraFragment();
    }
}
