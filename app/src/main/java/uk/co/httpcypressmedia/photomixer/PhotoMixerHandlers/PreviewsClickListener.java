package uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers;

public interface PreviewsClickListener {
    void onPreviewItemClick(int position);
}
