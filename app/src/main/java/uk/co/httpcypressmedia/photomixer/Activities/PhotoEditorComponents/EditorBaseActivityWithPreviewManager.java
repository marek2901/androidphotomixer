package uk.co.httpcypressmedia.photomixer.Activities.PhotoEditorComponents;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.gson.Gson;

import java.io.File;

import uk.co.httpcypressmedia.photomixer.PhotoMixerHandlers.ImageFilterHandler;
import uk.co.httpcypressmedia.photomixer.Activities.PhotoMixerMaterialToolbarActivity;


public abstract class EditorBaseActivityWithPreviewManager extends PhotoMixerMaterialToolbarActivity
        implements ImageFilterHandler {
    public static final String TAG = EditorBaseActivityWithPreviewManager.class.getSimpleName();
    private File photo;

    public File getPhoto() {
        return photo;
    }

    public void setPhoto(File photo) {
        this.photo = photo;
    }

    public File getFileFromIntent(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                return null;
            } else {
                return new Gson().fromJson(extras.getString(File.class.getSimpleName()), File.class);
            }
        } else {
            String jsonFileFromIntent = (String) savedInstanceState.getSerializable(File.class.getSimpleName());
            return new Gson().fromJson(jsonFileFromIntent, File.class);
        }
    }

    public void loadMainPhoto(int position) {
        try {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            new ImageFilter().getFilteredImage(
                    getPhoto(), this, position,
                    displaymetrics.widthPixels, displaymetrics.heightPixels);
        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
            onBackPressed();
        }
    }

    @Override
    public void imageFiltered(Bitmap image) {
        onNewImage(image);
    }

    public abstract void onNewImage(Bitmap bitmap);
}
